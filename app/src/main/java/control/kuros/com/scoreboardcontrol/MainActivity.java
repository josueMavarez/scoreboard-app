package control.kuros.com.scoreboardcontrol;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import org.w3c.dom.Text;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;


public class MainActivity extends Activity {

    private Socket client;
    private PrintWriter printwriter;
    private TextView redScore;
    private TextView orangeScore;
    private TextView greenScore;
    private TextView yellowScore;
    public String message;
    public String scor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton greenUp = (ImageButton) findViewById(R.id.greenUp);
        ImageButton greenDown = (ImageButton) findViewById(R.id.greenDown);
        ImageButton yellowUp = (ImageButton) findViewById(R.id.yellowUp);
        ImageButton yellowDown = (ImageButton) findViewById(R.id.yellowDown);
        ImageButton orangeUp = (ImageButton) findViewById(R.id.orangeUp);
        ImageButton orangeDown = (ImageButton) findViewById(R.id.orangeDown);
        ImageButton redUp = (ImageButton) findViewById(R.id.redUp);
        ImageButton redDown = (ImageButton) findViewById(R.id.redDown);
        Button dingBtn = (Button) findViewById(R.id.dingBtn);
        redScore = (TextView) findViewById(R.id.redScore);
        orangeScore = (TextView) findViewById(R.id.orangeScore);
        greenScore = (TextView) findViewById(R.id.greenScore);
        yellowScore = (TextView) findViewById(R.id.yellowScore);

        dingBtn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "dingBtn";
                new SendMessage().execute();
            }
        });


        redUp.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "redUp";
                new SendMessage().execute();
            }
        });

        redDown.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "redDown";
                new SendMessage().execute();
            }
        });

        greenUp.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "greenUp";
                new SendMessage().execute();
            }
        });

        greenDown.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "greenDown";
                new SendMessage().execute();
            }
        });

        yellowUp.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "yellowUp";
                new SendMessage().execute();
            }
        });

        yellowDown.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "yellowDown";
                new SendMessage().execute();
            }
        });

        orangeUp.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "orangeUp";
                new SendMessage().execute();
            }
        });

        orangeDown.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                message = "orangeDown";
                new SendMessage().execute();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private class SendMessage extends AsyncTask<String, Void, String> {

        String scored[];

        @Override
        protected String doInBackground(String... params) {
            try {
                client = new Socket("192.168.11.23", 4444);
                printwriter = new PrintWriter(client.getOutputStream(), true);
                printwriter.write(message+"\n");
                printwriter.flush();

                InputStream is = client.getInputStream();
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String.valueOf(scor = br.readLine());

                client.close();
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return scor;
        }

        protected void onProgressUpdate(Void... progress) {
        }

        protected void onPostExecute(String scor){
            scored = scor.split("-");
            String verde = scored[0];
            String amarrillo = scored[1];
            String naranja = scored[2];
            String rojo = scored[3];
            greenScore.setText(verde);
            yellowScore.setText(amarrillo);
            orangeScore.setText(naranja);
            redScore.setText(rojo);
        }
    }
}
